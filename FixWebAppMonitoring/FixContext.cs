﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FixWebAppMonitoring
{
    public class FixContext : FixBaseContext
    {
        public FixContext(): 
            base(new SqlConnection(ConfigurationManager.ConnectionStrings["local_db"].ConnectionString))
        {
            Database.SetInitializer(new FixInit());
        }
    }


    class FixInit : MigrateDatabaseToLatestVersion
        <FixContext, Migrations.Configuration>
    {
    }
}