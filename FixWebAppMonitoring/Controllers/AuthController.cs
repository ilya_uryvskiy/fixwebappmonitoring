﻿using FixWebAppMonitoring.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixWebAppMonitoring.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private IAuthManager _authManager;
        public AuthController(IAuthManagerFactory authManagerFactory)
        {
            _authManager = authManagerFactory.CreateAuthManager();
        }

        [HttpPost]
        public ActionResult Login(string login, string password)
        {
            _authManager.Login(login, password);
            return Redirect("/");
        }

        [HttpPost]
        public ActionResult Logout()
        {
            _authManager.Logout();
            return Redirect("/");
        }
    }
}