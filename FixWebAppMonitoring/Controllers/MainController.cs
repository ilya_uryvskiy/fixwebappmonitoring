﻿using FixWebAppMonitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixWebAppMonitoring.Controllers
{
    [AllowAnonymous]
    public class MainController : Controller
    {
        private IServerService _serverService;

        public MainController(IServerService serverService)
        {
            _serverService = serverService;
        }

        // GET: Main
        public ActionResult ServerList()
        {
            var services = _serverService.GetAllServers();
            return View(services);
        }
    }
}