﻿using FixWebAppMonitoring.Models;
using FixWebAppMonitoring.Services;
using FixWebAppMonitoring.WD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixWebAppMonitoring.Controllers
{
    public class AdminController : Controller
    {
        private IServerService _serverService;
        private IWatchDog _watchDog;

        public AdminController(IServerService serverService, IWatchDog watchDog)
        {
            _serverService = serverService;
            _watchDog = watchDog;
        }

        public ActionResult EditOrDeleteServer(int id)
        {
            Server server = _serverService.GetById(id);
            return View(server);
        }

        [HttpPost]
        public ActionResult EditOrDeleteServer(int id, string name, string probeUrl, int probeIntervalSec, string action)
        {
            _watchDog.DeleteServer(id);
            if (action == "Edit")
            {
                _serverService.Update(id, name, probeUrl, probeIntervalSec);
                _watchDog.AddOrUpdateServer(id, probeUrl, probeIntervalSec);
            }
            else if(action == "Delete")
            {
                _serverService.Delete(id);
            }
            return Redirect("/");
        }

        public ActionResult CreateServer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateServer(string name, string probeUrl, int probeIntervalSec)
        {
            Server server = _serverService.CreateNew(name, probeUrl, probeIntervalSec);
            _watchDog.AddOrUpdateServer(server.Id, server.ProbeUrl, server.ProbeIntervalSec);
            return Redirect("/");
        }
        
    }
}