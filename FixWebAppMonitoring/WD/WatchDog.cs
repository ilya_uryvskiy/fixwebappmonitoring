﻿using FixWebAppMonitoring.Models;
using FixWebAppMonitoring.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace FixWebAppMonitoring.WD
{
    public class WatchDog : IWatchDog
    {
        private ConcurrentDictionary<int, Timer> _timers = new ConcurrentDictionary<int, Timer>();
        private IServerProber _serverProber;
        private IServerService _serverService;

        public WatchDog(IServerProber serverProber, IServerService serverService)
        {
            _serverProber = serverProber;
            _serverService = serverService;
        }

        public void AddOrUpdateServer(int serverId, string probeUrl, int probeIntervalSec)
        {
            Timer timer = new Timer(_ =>
            {
                ServerStatus serverStatus = _serverProber.IsAvailable(probeUrl) 
                ? ServerStatus.Up : ServerStatus.Down;
                _serverService.UpdateServerStatus(serverId, serverStatus);
            }, null, 0, probeIntervalSec * 1000);

            _timers.AddOrUpdate(serverId, timer, (id, existingTimer) =>
            {
                existingTimer.Dispose();
                return timer;
            });
        }

        public void DeleteServer(int serverId)
        {
            Timer timer;
            if (_timers.TryRemove(serverId, out timer))
            {
                timer.Dispose();
            }
        }
    }

    public interface IWatchDog
    {
        void AddOrUpdateServer(int serverId, string probeUrl, int probeIntervalSec);

        void DeleteServer(int serverId);
    }
}