﻿namespace FixWebAppMonitoring.WD
{
    public interface IServerProber
    {
        bool IsAvailable(string url);
    }
}