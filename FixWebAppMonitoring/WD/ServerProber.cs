﻿using System.Net.Http;

namespace FixWebAppMonitoring.WD
{
    public class ServerProber : IServerProber
    {
        private HttpClient _httpClient = new HttpClient(new HttpClientHandler()
        {
            //AllowAutoRedirect = false
        });

        public bool IsAvailable(string url)
        {
            bool success = true;
            try
            {
                _httpClient.GetAsync(url)
                    .Result.EnsureSuccessStatusCode();
            }
            catch
            {
                success = false;
            }

            return success;
        }
    }
}