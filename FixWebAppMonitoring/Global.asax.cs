﻿using FixWebAppMonitoring.Auth;
using FixWebAppMonitoring.Models;
using FixWebAppMonitoring.Services;
using FixWebAppMonitoring.WD;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FixWebAppMonitoring
{
    public class MvcApplication : System.Web.HttpApplication
    {
        Container container = new Container();

        protected void Application_Start()
        {
            container.Register<IWatchDog, WatchDog>(Lifestyle.Singleton);
            container.Register<IServerProber, ServerProber>(Lifestyle.Singleton);
            container.Register<IFixContextFactory, FixContextFactory>(Lifestyle.Singleton);
            container.Register<IServerService, ServerService>(Lifestyle.Singleton);
            container.Register<IAuthManagerFactory, AuthManagerFactory>(Lifestyle.Singleton);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalFilters.Filters.Add(container.GetInstance<FixAuthorizeAttribute>());


            AddServicesToWatchDog();
        }

        private void AddServicesToWatchDog()
        {
            IWatchDog watchDog = container.GetInstance<IWatchDog>();
            IServerService serverService = container.GetInstance<IServerService>();
            foreach (Server server in serverService.GetAllServers())
            {
                watchDog.AddOrUpdateServer(server.Id, server.ProbeUrl, server.ProbeIntervalSec);
            }
        }
    }
}
