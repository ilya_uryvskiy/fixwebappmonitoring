﻿using FixWebAppMonitoring.Models;
using System.Collections.Generic;

namespace FixWebAppMonitoring.Services
{
    public interface IServerService
    {
        IEnumerable<Server> GetAllServers();
        IEnumerable<Server> GetOrderedById(int take, int skip);
        Server GetById(int serverId);
        Server CreateNew(string name, string probeUrl, int probeIntervalSec);
        Server Update(int serverId, string name, string probeUrl, int probeIntervalSec);       
        void UpdateServerStatus(int serverId, ServerStatus newStatus);
        void Delete(int serverId);
    }
}