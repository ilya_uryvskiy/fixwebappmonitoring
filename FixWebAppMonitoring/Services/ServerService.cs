﻿using FixWebAppMonitoring.Models;
using FixWebAppMonitoring.WD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixWebAppMonitoring.Services
{
    public class ServerService : IServerService
    {
        private IFixContextFactory _contextFactory;

        public ServerService(IFixContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public IEnumerable<Server> GetAllServers()
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                return context.Servers.ToList();
            }
        }

        public IEnumerable<Server> GetOrderedById(int take, int skip)
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                return context.Servers.OrderBy(s => s.Id)
                    .Take(take).Skip(skip).ToList();
            }
        }

        public Server GetById(int serverId)
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                return context.Servers.FirstOrDefault(s => s.Id == serverId);
            }
        }

        public Server CreateNew(string name, string probeUrl, int probeIntervalSec)
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                Server server = new Server
                {
                    Name = name,
                    ProbeUrl = probeUrl,
                    ProbeIntervalSec = probeIntervalSec
                };

                context.Servers.Add(server);
                context.SaveChanges();
                return server;
            }

        }

        public Server Update(int serverId, string name, string probeUrl, int probeIntervalSec)
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                Server server = context.Servers.FirstOrDefault(serv => serv.Id == serverId);
                if (server != null)
                {
                    server.Name = name;
                    server.ProbeUrl = probeUrl;
                    server.ProbeIntervalSec = probeIntervalSec;

                    context.SaveChanges();
                }
                return server;
            }

        }



        public void UpdateServerStatus(int serverId, ServerStatus newStatus)
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                Server server = context.Servers.FirstOrDefault(x => x.Id == serverId);
                if (server != null)
                {
                    DateTimeOffset now = DateTimeOffset.Now;
                    server.Status = newStatus;
                    server.ProbeDt = now;

                    ServerProbeHistory probeHistory = new ServerProbeHistory
                    {
                        Server = server,
                        Status = newStatus,
                        ProbeDt = now
                    };
                    context.ServerProbeHistories.Add(probeHistory);

                    context.SaveChanges();
                }
            }

        }


        public void Delete(int serverId)
        {
            using (FixBaseContext context = _contextFactory.CreateFixContext())
            {
                Server server = context.Servers.FirstOrDefault(s => s.Id == serverId);
                if(server!= null)
                {
                    context.Entry(server).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }
    }
}