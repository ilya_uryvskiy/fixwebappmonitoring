namespace FixWebAppMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServerProbeHistoryEntityAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ServerProbeHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Byte(nullable: false),
                        ProbeDt = c.DateTimeOffset(nullable: false, precision: 7),
                        Server_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Servers", t => t.Server_Id)
                .Index(t => t.Server_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServerProbeHistories", "Server_Id", "dbo.Servers");
            DropIndex("dbo.ServerProbeHistories", new[] { "Server_Id" });
            DropTable("dbo.ServerProbeHistories");
        }
    }
}
