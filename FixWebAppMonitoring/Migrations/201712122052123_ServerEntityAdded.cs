namespace FixWebAppMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServerEntityAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Servers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 32),
                        ProbeUrl = c.String(maxLength: 256),
                        ProbeIntervalSec = c.Int(nullable: false),
                        Status = c.Byte(),
                        ProbeDt = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Servers");
        }
    }
}
