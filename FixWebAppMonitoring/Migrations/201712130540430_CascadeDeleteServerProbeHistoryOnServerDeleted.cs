namespace FixWebAppMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeDeleteServerProbeHistoryOnServerDeleted : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ServerProbeHistories", "Server_Id", "dbo.Servers");
            DropIndex("dbo.ServerProbeHistories", new[] { "Server_Id" });
            AlterColumn("dbo.ServerProbeHistories", "Server_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.ServerProbeHistories", "Server_Id");
            AddForeignKey("dbo.ServerProbeHistories", "Server_Id", "dbo.Servers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServerProbeHistories", "Server_Id", "dbo.Servers");
            DropIndex("dbo.ServerProbeHistories", new[] { "Server_Id" });
            AlterColumn("dbo.ServerProbeHistories", "Server_Id", c => c.Int());
            CreateIndex("dbo.ServerProbeHistories", "Server_Id");
            AddForeignKey("dbo.ServerProbeHistories", "Server_Id", "dbo.Servers", "Id");
        }
    }
}
