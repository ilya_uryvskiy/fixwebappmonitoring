﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixWebAppMonitoring
{
    public interface IFixContextFactory
    {
        FixBaseContext CreateFixContext();
    }

    public class FixContextFactory : IFixContextFactory
    {
        public FixBaseContext CreateFixContext()
        {
            return new FixContext();
        }
    }
}