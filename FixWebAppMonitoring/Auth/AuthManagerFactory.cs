﻿using System.Web;

namespace FixWebAppMonitoring.Auth
{
    public class AuthManagerFactory : IAuthManagerFactory
    {
        public IAuthManager CreateAuthManager()
        {
            return new AuthManager(HttpContext.Current);
        }
    }
}
