﻿using System.Security.Principal;

namespace FixWebAppMonitoring.Auth
{
    public class FixPrincipal : IPrincipal
    {
        public FixPrincipal(string login)
        {
            Identity = new FixIdentity(login);
        }

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role) => false;
    }
}