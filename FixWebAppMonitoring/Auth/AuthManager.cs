﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace FixWebAppMonitoring.Auth
{
    public class AuthManager : IAuthManager
    {
        private const string AuthCookieName = "AUTH";

        private const string LoginHardCode = "Admin";
        private const string PasswordHardCode = "P@ssw0rd";

        private HttpContext _httpContext;

        public AuthManager(HttpContext httpContext)
        {
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
        }

        public void Login(string login, string password)
        {
            if (string.Equals(login, LoginHardCode, StringComparison.InvariantCultureIgnoreCase)
                && password == PasswordHardCode)
            {
                _httpContext.Response.Cookies.Add(CreateCookie(login));
            }
        }

        public void Logout()
        {
            var httpCookie = _httpContext.Response.Cookies[AuthCookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
            }
        }

        public IPrincipal CurrentUser
        {
            get
            {
                try
                {
                    HttpCookie authCookie = _httpContext.Request.Cookies.Get(AuthCookieName);
                    if(authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
                    {
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                        return new FixPrincipal(ticket.Name);
                    }
                    else
                    {
                        return new FixPrincipal(null);
                    }
                }
                catch
                {
                    return new FixPrincipal(null);
                }
            }
        }


        private HttpCookie CreateCookie(string login, bool isPersistent = true)
        {
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                  1, login, DateTime.Now, DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent, string.Empty, FormsAuthentication.FormsCookiePath);

            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);

            HttpCookie cookie = new HttpCookie(AuthCookieName)
            {
                Value = encryptedTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            return cookie;
        }
    }
}
