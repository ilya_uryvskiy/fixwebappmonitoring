﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace FixWebAppMonitoring.Auth
{
    public interface IAuthManager
    {
        void Login(string login, string password);

        void Logout();

        IPrincipal CurrentUser { get; }
    }
}
