﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace FixWebAppMonitoring.Auth
{
    public class FixIdentity : IIdentity
    {
        private string _login;

        public FixIdentity(string login)
        {
            _login = login;
        }

        public string Name => _login ?? "Anonymous";

        public string AuthenticationType => "Custom";

        public bool IsAuthenticated => _login != null;
    }
}