﻿namespace FixWebAppMonitoring.Auth
{
    public interface IAuthManagerFactory
    {
        IAuthManager CreateAuthManager();
    }
}
