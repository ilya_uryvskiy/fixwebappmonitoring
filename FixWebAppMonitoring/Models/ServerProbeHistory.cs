﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixWebAppMonitoring.Models
{
    public class ServerProbeHistory
    {
        public int Id { get; set; }

        public ServerStatus Status { get; set; }

        public DateTimeOffset ProbeDt { get; set; }

        public virtual Server Server { get; set; }
    }
}