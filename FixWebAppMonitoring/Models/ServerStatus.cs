﻿namespace FixWebAppMonitoring.Models
{
    public enum ServerStatus : byte
    {
        Up = 0x00,
        Down = 0x01
    }
}