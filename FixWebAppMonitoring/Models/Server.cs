﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FixWebAppMonitoring.Models
{
    public class Server
    {
        public Server()
        {
            ServerProbeHistories = new HashSet<ServerProbeHistory>();
        }

        public int Id { get; set; }

        [MaxLength(32)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string ProbeUrl { get; set; }

        public int ProbeIntervalSec { get; set; }

        public ServerStatus? Status { get; set; }

        public DateTimeOffset? ProbeDt { get; set; }

        public virtual ICollection<ServerProbeHistory> ServerProbeHistories { get; set; }
    }
}