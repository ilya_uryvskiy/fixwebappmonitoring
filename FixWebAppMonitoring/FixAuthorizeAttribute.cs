﻿using FixWebAppMonitoring.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixWebAppMonitoring
{
    public class FixAuthorizeAttribute : AuthorizeAttribute
    {
        private IAuthManagerFactory _authManagerFactory;
        public FixAuthorizeAttribute(IAuthManagerFactory authManagerFactory)
        {
            _authManagerFactory = authManagerFactory;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.User = _authManagerFactory.CreateAuthManager().CurrentUser;
            base.OnAuthorization(filterContext);
        }
        
    }
}