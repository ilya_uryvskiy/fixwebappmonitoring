﻿using FixWebAppMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FixWebAppMonitoring
{
    public class FixBaseContext : DbContext
    {
        public FixBaseContext(DbConnection connection) : base(connection, true)
        {
        }

        public DbSet<Server> Servers { get; set; }

        public DbSet<ServerProbeHistory> ServerProbeHistories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServerProbeHistory>()
                .HasRequired(sph => sph.Server)
                .WithMany(s => s.ServerProbeHistories)
                .WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);
        }
    }

}